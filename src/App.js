import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import HeaderBar from './components/HeaderBar';
import FooterBar from './components/FooterBar';

/**
 * @TODO: Override the primary styles
 */
const theme = createMuiTheme({
  direction: 'ltr', // Both here and <body dir="rtl">
});

const App = props => ({
  render() {
    return (
      <MuiThemeProvider theme={theme}>
         <div dir="rtl">
            <HeaderBar/>
        <div>{props.children}</div>
            <FooterBar/>
          </div>
      </MuiThemeProvider>
    );
  }
});

export default App;
