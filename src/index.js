import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import routes from './config/routes';

ReactDOM.render(
  routes,
  document.getElementById('root')
);
