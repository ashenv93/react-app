  import React, {Component} from 'react';
  import PropTypes from 'prop-types';

  import classNames from 'classnames';
  import { withStyles } from '@material-ui/core/styles';
  import Drawer from '@material-ui/core/Drawer';
  import AppBar from '@material-ui/core/AppBar';
  import Toolbar from '@material-ui/core/Toolbar';
  import List from '@material-ui/core/List';
  import CssBaseline from '@material-ui/core/CssBaseline';
  import Typography from '@material-ui/core/Typography';
  import Divider from '@material-ui/core/Divider';
  import IconButton from '@material-ui/core/IconButton';
  import MenuIcon from '@material-ui/icons/Menu';
  import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
  import ListItem from '@material-ui/core/ListItem';
  import ListItemText from '@material-ui/core/ListItemText';
  import InboxIcon from '@material-ui/icons/MoveToInbox';
  import MailIcon from '@material-ui/icons/Mail';

  import Dashboard from './Dashboard';
  import Summary from './Summary';

  const drawerWidth = 240;
  /**
   * Describes the style elements on the Home page
   * @TODO : Use a seperate style file (Syntactically Awesome Style Sheet) to retrive the relevent style elements
   */
  const styles = theme => ({
    root: {
        display: 'flex',
        overflow: 'hidden'
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
      },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
  },
    drawerClose: {
        transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,}),
        overflowX: 'hidden',
        width: theme.spacing.unit * 7 + 1,
        [theme.breakpoints.up('sm')]: {
        width: theme.spacing.unit * 9 + 1,
      },
    },
  toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    contentOpen: {
        flexGrow: 1,
        padding: '10px 0px 0px 100px',
    },
  contentClose: {
      flexGrow: 1,
      padding: '10px 0px 0px 280px'
    },
    listText: {
        padding: '0px 33px 0px 0px'
      },
  });

  let content = (
      <div>
         Application Title
      </div>
    );
    
  /**
   * Represents the view logic of drawer functionality
   */  
  class Home extends Component {
    /**
     * Class Constructor
     */
      constructor () {
          super();
          this.state = {
          open: false,
      }

      this.changeState = this.changeState.bind(this);
      this.changeContentDashboard = this.changeContentDashboard.bind(this);
      this.changeContentSummary = this.changeContentSummary.bind(this);
    }
    
  /**
   * Change the state according to the click event
   */
  changeState () {
      this.setState({ open: !this.state.open });
      content = (
      <div>
         Application Title
      </div>
    );
  };
  /**
   * Change the content to dashboard
   */
  changeContentDashboard() {
      this.setState({ open: !this.state.open });
      content = (
      <div>
         <Dashboard/>
      </div>
      );
  };
  /**
   * Change the content to summary
   */
  changeContentSummary() {
    this.setState({ open: !this.state.open });
      content = (
      <div>
         <Summary/>
      </div>
      );
  };

  /**
   * Describes the elements on the Home page
   * @return {String} HTML elements
   */
  render(){
    
  const { classes } = this.props;
  return(
  <div className={classes.root}>
     <CssBaseline />
         <AppBar
         position="absolute"
         className={classNames(classes.appBar, {
         [classes.appBarShift]: this.state.open,
         })}
         >
           <Toolbar disableGutters={!this.state.open} dir="ltr">
                <IconButton
                color="inherit"
                aria-label="Open drawer"
                onClick={this.changeState}
                className={classNames(classes.menuButton, {
                [classes.hide]: this.state.open,
                })}
                >
                <MenuIcon />
                </IconButton>
                <Typography variant="h5" color="inherit" noWrap>
                   Mini variant drawer
                </Typography>
           </Toolbar>
         </AppBar>
     <Drawer
     variant="permanent"
     className={classNames(classes.drawer, {
     [classes.drawerOpen]: !this.state.open,
     [classes.drawerClose]: this.state.open,
     })}
     classes={{
     paper: classNames({
     [classes.drawerOpen]: this.state.open,
     [classes.drawerClose]: !this.state.open,
     }),
     }}
     open={this.state.open}
     >
     <div className={classes.toolbar}>
        <IconButton onClick={this.changeState}>
           <ChevronLeftIcon />
        </IconButton>
     </div>
     <Divider />
     <List>
        <ListItem onClick={this.changeContentDashboard}>
           <InboxIcon className ={classes.listIcon} />
           <ListItemText className ={classes.listText} primary="Dashboard"/>
        </ListItem>
     </List>
     <Divider />
     <List>
        <ListItem onClick={this.changeContentSummary}>
           <MailIcon className ={classes.listIcon} />
           <ListItemText  className ={classes.listText} primary="Summary" />
        </ListItem>
     </List>
     <Divider />
     </Drawer>
     <div className={classNames( {
     [classes.contentClose]: this.state.open,
     [classes.contentOpen]: !this.state.open,
     })}>
     <div className={classes.toolbar} />
        {content}
     </div>
  </div>
    );
  }
}
  Home.propTypes = {
  classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(Home);