export const  APP_STRING_CONST = {

  DATA_LOADING: 'Please wait...',

  APP_NAME: 'REACT_BOILERPLATE',

  DEFAULT_IMAGE: 'https://upload.wikimedia.org/wikipedia/commons/a/a7/React-icon.svg'
}
