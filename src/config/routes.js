import {Route} from "react-router";
import React from "react";
import { Router } from 'react-router';
import App from '../App';
import Home from '../containers/Home';
import Dashboard from '../containers/Dashboard';
import Summary from '../containers/Summary';
import Backoffice from '../containers/Backoffice';
import Transaction from '../containers/Transaction';
import history from '../history';

/**
  * TODO: Create a fallback component to add a switch tag 
*/
const routes = (
<App>
   <Router history={history}>
      <div>
         <Route exact path="/" component={ Home } />
         <Route exact path="/dashboard" component={ Dashboard } />
         <Route exact path="/summary" component={ Summary } />
         <Route exact path="/backoffice" component={ Backoffice } />
         <Route exact path="/transaction" component={ Transaction } />
      </div>
   </Router>
</App>
);
export default routes;